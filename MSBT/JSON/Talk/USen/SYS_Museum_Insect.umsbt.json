﻿[
  {
    "label": "001",
    "text": "Seen flapping in the sun, common\nbutterflies can be identified by\nblack spots on their white wings.\nTo the human eye, both males and\nfemales look white, but there are\nactually subtle differences.\nOn closer inspection, the males'\nwings look black while the females'\nwings look white.\nThe caterpillars have been known to\ncause damage to farmers' crops."
  },
  {
    "label": "002",
    "text": "The yellow butterfly is similar to\nthe common butterfly, with the two\neasily mistaken for one another.\nMales are more often a yellowish\ncolor, and females are more often\nwhite.\nThe larvae feed on plants of the\nFabaceae family, such as milk vetch\nor white clover."
  },
  {
    "label": "003",
    "text": "The larvae of tiger butterflies feed\non leaves of orange trees until\nthey metamorphose.\nWhen they're threatened, they emit\na foul smell from their antennae to\nprotect themselves.\nThe dusting you see on your fingers\nwhenever you touch their wings is\nactually from their scales.\nThese scales form the wing pattern\nas well as repel rainwater, making\nthem very important for survival."
  },
  {
    "label": "004",
    "text": "The back of the wings of a peacock\nbutterfly is black, while the front is\nblue or green.\nThe males have black, velvet hair on\ntheir forewings and are easy to spot\nbecause of their beauty.\nSometimes you may see a group of\nmale peacock butterflies drinking\nwater together."
  },
  {
    "label": "005",
    "text": "Monarch butterflies are known for\ntheir lengthy southward migrations\nin the winter.\nOnce spring rolls around, they\nreturn to their original homes until\nthe weather turns cold again.\nIn their southern habitat during\nwinter, you may see thousands\nof them covering the trees."
  },
  {
    "label": "006",
    "text": "Emperor butterflies are easily\nrecognized because of their lovely,\niridescent blue wings.\nPigmentation isn't actually what\ncauses the color, but rather the\nreflections on their wing scales.\nSince their wings shimmer when\nmoving, noticing them is much\neasier when they are midflight."
  },
  {
    "label": "007",
    "text": "Agrias butterflies are argued to\nhave the most beautiful wings of\nall butterfly species.\nThey're capable of flying quite fast,\nmaking them more difficult to catch\nthan a number of their peers.\nTheir wings sport three colors—red,\nblue, and black—but many have\ndifferent patterns and shapes."
  },
  {
    "label": "008",
    "text": "Raja Brooke butterflies have red\nheads and a red strip around their\nnecks, giving the look of a collar.\nThey are known to drink hot-spring\nwater and often hang around water\nflowing up from the ground.\nWhile males are vibrantly green,\nfemales are much more commonly\nseen with brown wings."
  },
  {
    "label": "009",
    "text": "The world's largest butterflies,\nbirdwing butterflies, have wings\nthat can measure 30 cm long.\nThe females have longer wings than\nthe males, while the males' wings\nhave a deeper blue color.\nThough they are extremely popular\nwith collectors, international trading\nis either limited or fully banned."
  },
  {
    "label": "010",
    "text": "Moths are related to butterflies,\nbut they're most often active\nat night instead of day.\nThey use their large antennae to\nsense, smell, and guide themselves\nin the dark.\nInterestingly, the number of moth\nspecies is estimated at 160,000, so\n\"moth\" is a fairly broad descriptor."
  },
  {
    "label": "011",
    "text": "Oak silk moths are seen in the\nsummer and are one of the largest\nmoth species in the world.\nThey make silk from their mouths\nas they transition into the pupa\nstage.\nAs adults, they don't eat anything\nand only live off the nutrients they\nstored while they were caterpillars."
  },
  {
    "label": "012",
    "text": "Honeybees gather nectar from\nflowers and make honey, making\nthem integral in many ecosystems.\nThe worker bees are females and\nthe only ones capable of stinging,\nthough that is rather rare.\nThe hive does contain males, but\nafter mating season, they are all\nsent away.\nHoney has been consumed since\nancient times. Cave paintings 8,000\nyears old show people eating honey."
  },
  {
    "label": "013",
    "text": "Bees are prone to attacking anyone\nwho comes too close to their hive,\nso be careful when approaching!\nThe yellow and black you see on\ntheir bodies are colors often used\nto convey danger.\nThis danger is pretty significant, as\nmany varieties of bees are capable\nof stinging multiple times."
  },
  {
    "label": "014",
    "text": "When male long locusts jump, you\ncan hear the trademark stridulation\nnoise they make.\nLong locusts can be green as well as\nbrown. The brown ones usually hide\nnear dried grass or brown areas.\nWhile males are long, females are\nconsistently much longer, making\nit simple to tell them apart."
  },
  {
    "label": "015",
    "text": "Migratory locusts are large insects\nthat can jump up to 32 feet, as they\nopen their wings when they jump.\nThese locusts love to eat and can\nconsume their own weight in food\nevery single day.\nAs a group, their hunger is enough\nthat a million locusts can consume\none ton of food per day.\nOddly, when migratory locusts grow\nup surrounded only by other locusts,\ntheir shells will be entirely black."
  },
  {
    "label": "016",
    "text": "Rice grasshoppers are typically\nknown as pests that eat farmers'\ncrops, making them undesirable.\nBut they're also edible, and boiling\nthem in soy sauce is a typical way\nto prepare them in certain regions.\nWhile they are generally green, their\nshells will turn a darker color when\nheavily crowded in groups."
  },
  {
    "label": "017",
    "text": "Mantises are known for the praying\npose they sit in while ambushing\nunsuspecting prey.\nThey'll snatch whatever is moving\nnearby and can even capture\ncreatures larger than themselves.\nWhen threatened, mantises will\nspread their thin green wings to\nappear more intimidating."
  },
  {
    "label": "018",
    "text": "Orchid mantises look like orchids\nand use them as camouflage for\nprotection from predators.\nWhile this camouflage keeps them\nsafe, it also helps them to surprise\nany prey that comes to the flowers.\nThey are usually carnivorous, but\nthey still eat the occasional banana\nto balance their vitamin intake."
  },
  {
    "label": "019",
    "text": "The crying of brown cicadas is\nusually associated with the sound\nof hot oil sputtering.\nTheir wings are brown instead of\nbeing transparent, making them a\nrare species throughout the world.\nWhile completely harmless by\nthemselves, in swarms they can\ncause devastation to crops."
  },
  {
    "label": "020",
    "text": "Robust cicadas are distinguished by\nthe shortness of their bodies in\nrelation to their wing size.\nThey used to share territories with\nbrown cicadas, but robust cicadas\ntend to prefer dry locales.\nAs you'd expect, they make a rather\nrobust call, though males actually\nchange their sound around females."
  },
  {
    "label": "021",
    "text": "As the name might indicate, giant\ncicadas are one of the largest\nspecies of cicada in the world.\nThey used to live mostly in warmer\nwestern Japan, but they've now\nalso moved to urban eastern Japan.\nThough they're now common in\nthese areas, they aren't well known\nin other parts of the world."
  },
  {
    "label": "022",
    "text": "Walker cicadas have a unique crying\nsound that gives the impression\nthey're all singing in harmony.\nThey usually start crying late in the\nsummer, so people can associate\ntheir sound with summer's end.\nThis call is so related with summer\nin general, TV shows regularly use\nthis cicada to indicate the season."
  },
  {
    "label": "023",
    "text": "Evening cicadas cry when it's dark\nout, regardless of what time of day\nit actually is.\nPeople tend to think it's the end of\nsummer when they hear the cry of\nevening cicadas.\nHowever, this species of cicada\nstarts crying at the end of the rainy\nseason, which generally falls in July."
  },
  {
    "label": "024",
    "text": "Cicada larvae emerge from the\nground in the evening and molt on\ntrees, leaving behind empty shells.\nIt's possible to find out the species\nof cicadas just by looking at these\nshells.\nCollecting the shells will tell you\nhow many and what kind of cicadas\ninhabit a certain area."
  },
  {
    "label": "025",
    "text": "Lantern flies stay on trees to drink\ntree sap, which is where you'll most\ncommonly find them.\nTheir heads are shaped like\nalligators, though this is just\nan odd coincidence.\nTheir wings have patterns that look\nlike eyes so when they're open they\ncan scare off predators.\nThey don't really cry that much,\nbut they are still related to cicadas\nto some extent."
  },
  {
    "label": "026",
    "text": "Red dragonflies are seen in the fall,\nas they're rather sensitive to the\nheat of summer.\nThey stay in the mountains during\nsummer and come down to the\nplains in cooler weather.\nThe males' bodies turn red when\nthey mature, but their heads and\nthoraxes are brownish.\nThe females have a more yellowish\ncolor and aren't as red as the males,\nmaking them easy to tell apart."
  },
  {
    "label": "027",
    "text": "The heads and thoraxes of darner\ndragonflies are colored a lovely\nyellow green.\nThe males have a light-blue spot on\ntheir bellies, which is not a trait the\nfemales share.\nDarner dragonflies can usually be\nseen flying around bodies of water\nsuch as lakes or ponds."
  },
  {
    "label": "028",
    "text": "Banded dragonflies have black-\nand-yellow-striped bodies and\nbright-green eyes.\nThey are large and can fly quite\nfast. They have strong jaws, so it\nmight be painful if you're bitten.\nTheir larval stage lasts two to four\nyears, where they live near water,\nfeeding on tadpoles and small fish."
  },
  {
    "label": "029",
    "text": "Dragonflies very similar to petaltail\ndragonflies have been found in\nfossils from the Jurassic period.\nThey are considered \"living fossils\"\nthat haven't changed much since\nprehistoric times.\nTheir wingspan is over 16 cm,\nmaking them the world's largest\ndragonfly."
  },
  {
    "label": "030",
    "text": "Ants are small but very powerful\nand are able to carry items far\nheavier than they are.\nIn the nest, there is a queen and\nalso worker ants, making them\nsomewhat similar to bees.\nIn some habitats, ants actually make\nup between 15-20% of the area's\ntotal terrestrial-animal biomass."
  },
  {
    "label": "031",
    "text": "Pondskaters distinguish themselves\nby having the ability to run on the\nsurface of water.\nThey have a mouth part that lets\nthem suck up bugs that fall on the\nwater's surface.\nTheir wings allow them to freely\nmove to different parts of the\nwater's surface if the need arises.\nSome say they smell like candy,\nwhich is a rather curious association\nto make in the insect world."
  },
  {
    "label": "032",
    "text": "Diving beetles swim using thick,\nhairy hind legs and clean the water\nby eating dead insects.\nThey store a supply of air under\ntheir wings to breathe underwater\nand surface to replenish as needed.\nWhen they're caught by predators,\nthey release a foul-smelling bluish\nfluid from their heads in defense."
  },
  {
    "label": "033",
    "text": "Stinkbugs are known for their foul\nsmell, enough to drive away most\npredators and people.\nThe smell is so strong that if they\nrelease it in an airtight place, they\npass out from their own smell.\nThey like to gather in clumps to\nstay warm in sunshine, though they\nwill seek out warm houses as well."
  },
  {
    "label": "034",
    "text": "Snails are mollusks that live on land\nand are related to shellfish like\nclams and oysters.\nOne snail can play the role of both\na male and a female, so it can lay\neggs all by itself.\nNewly hatched snails already have a\ntiny shell, so right from birth they\nare easily identified.\nMost species have clockwise spirals\non their shells, though spirals that\ngo counterclockwise are possible."
  },
  {
    "label": "035",
    "text": "The chirping sound you often hear\nfrom male crickets is emitted by\nthe stridulation of their wings.\nCrickets' ears are located just\nbelow the middle joint of each front\nleg.\nWhile this seems odd to us, it helps\nthem easily pinpoint where various\nsounds are coming from."
  },
  {
    "label": "036",
    "text": "The chirping sound of bell crickets\nis considered beautiful and tends to\nrepresent the coming of fall.\nThis easily recognizable chirping is\ncreated when males of the species\nrub their forewings together.\nThe resulting sound is much higher\nin pitch than the human voice, so\nit can't be heard over a phone."
  },
  {
    "label": "037",
    "text": "Grasshoppers are known for their\nunique songs, but unlike crickets,\nit doesn't come from their wings.\nRather, the chirping sound comes\nfrom their rubbing their legs\ntogether.\nThey look like a thicker version\nof locusts, but some species of\ngrasshoppers actually eat locusts."
  },
  {
    "label": "038",
    "text": "Mole crickets live underground in\ntunnels they dig themselves, moving\nfreely through the soil.\nTheir forelimbs resemble those of\nmoles and are very suitable for the\ntask of digging.\nThey have large wings that allow\nthem to fly, but they can also swim \nif the need arises.\nIn the past, people used to think the\nnoise they made underground was\nactually coming from worms."
  },
  {
    "label": "039",
    "text": "Walking leaves look just like real\nleaves. They're related to walking\nsticks, which resemble twigs.\nThe females are better at mimicking\nleaves than the males, though they\ncan't fly.\nTheir mimicking is so thorough that\nsome of them even have fake bite\nmarks on their bodies."
  },
  {
    "label": "040",
    "text": "Walking sticks disguise themselves\nto confuse predators and are very\nclosely related to walking leaves.\nThey have cylindrical bodies that\nlook like sticks. Some of them can\neven change their pigmentation.\nFemales can lay eggs without the\nmales, so most of the walking sticks\nyou see are females."
  },
  {
    "label": "041",
    "text": "Bagworms are certain moths in\ncaterpillar phase. They stay in cases\nor cocoons for warmth in winter.\nThey construct their cases by\nsticking silk threads together\nbetween leaves or branches.\nFemales don't have wings, and some\nsimply wait inside their cocoons for\nmales to come by to mate."
  },
  {
    "label": "042",
    "text": "Despite the name, not all ladybugs\nare \"ladies.\" There are also male\nladybugs.\nThough you'll see different ladybugs\nwith different numbers of spots,\nthey don't get more as they age.\nLadybugs eat insects harmful to\ncrops. A few species eat the leaves\nof crops, but most are beneficial.\nThey're tough against the cold, but\nduring winter they stay in groups\nunder dry leaves for warmth."
  },
  {
    "label": "043",
    "text": "As the name indicates, these bugs\nresemble violins. They're very thin\nand roughly five mm thick.\nThey stay on top of fungi on trees\nand prey upon small bugs that\nhappen by.\nWhen predators come near, violin\nbeetles emit a noxious fluid to\nprotect themselves.\nThe fluid is toxic and can be painful\nif it touches your hands or eyes, so\nbe careful!"
  },
  {
    "label": "044",
    "text": "Longhorn beetles have very strong\nmandibles, and both adult and larval\nforms eat through the bark of trees.\nThey have extremely long antennae\nthat are sometimes even longer than\ntheir bodies.\nThe adults have such strong\nmandibles that they can bite off\npeople's hair.\nAnd keep in mind that hair isn't the\nonly thing they'll bite, so be careful\nwhen handling them!"
  },
  {
    "label": "045",
    "text": "Tiger beetles are known for their\naggressive predatory habits and\nfast running speed.\nTheir bodies are rather colorful and\nreflect the sunshine, giving them a\nshimmery appearance.\nTiger-beetle larvae live in cylindrical\nburrows and capture insects that\nwander past with their mandibles."
  },
  {
    "label": "046",
    "text": "Dung beetles are famous for the\nexact thing you'd assume they're\nfamous for.\nIn fact, they use their namesake\nboth as a source of food and a\nplace to lay their eggs.\nDung beetles are very strong and\ncan roll things 10 times the weight\nof their bodies.\nOne species can even pull objects\n1,141 times its weight, which is like\na human pulling 6 packed buses!"
  },
  {
    "label": "047",
    "text": "Wharf roaches can be seen along the\nsea, usually just above the waterline\non rocky cliffs.\nThey feed mainly on microalgae and\ndetritus that drift to the seashore,\nmaking them cleaners of sorts.\nThey often move in groups and run\naway quickly when they hear people\napproaching."
  },
  {
    "label": "048",
    "text": "Hermit crabs live in empty shells\nthey can fit their bodies into. As\nthey grow, they find bigger shells.\nThey're quite picky about each new\nshell and first measure the opening\nwith their claws.\nThe shells they choose usually fit\nwell, and they'll sometimes fight\none another over ownership rights."
  },
  {
    "label": "049",
    "text": "Both male and female fireflies are\ncapable of producing light from\ntheir lower abdomens.\nThis light isn't the same as the light\nfrom a lightbulb, though, and isn't\nhot when you touch it.\nFireflies can actually glow when\nin the egg and larval stages, but\nthat light is comparatively weaker."
  },
  {
    "label": "050",
    "text": "The fruit beetle has a brilliant-\ngreen-colored body and tends\nto hang around fruit trees.\nThey have a hard outer wing casing\nand lift this casing when they need\nto fly using their thin wings.\nThe larvae of fruit beetles feed\non decaying vegetable debris and\nplant roots."
  },
  {
    "label": "051",
    "text": "Scarab beetles have elegant,\nmetallic-looking bodies and are\nshinier even than fruit beetles.\nThey're so shiny that they're said to\ngleam like gold. They're very rare\nand can be sold for high prices.\nLike all beetles in the scarab family,\nthe outer shell provides a very thick\nlayer of protection from predators.\nThe high value and golden shell do\nmake them more vulnerable when it\ncomes to Bell hunters, though."
  },
  {
    "label": "052",
    "text": "Jewel beetles are very pretty\ninsects that show a red line on their\ngreen bodies when refracting light.\nThey're so pretty that it's believed\npeople used them for craftwork a\nlong time ago.\nTheir shiny wings are easy to notice,\nbut birds have a hard time targeting\nthem.\nThis is because birds aren't very\ngood at seeing things that change\ncolor in different lights."
  },
  {
    "label": "053",
    "text": "Miyama stags look strong, with\ntheir thick, bumpy exoskeletons and\nyellow hair covering their bodies.\nThey're active during the day but\nreally don't prefer the heat during\nthe summer.\nContrary to their appearance, they\nare rather delicate insects, so be\nsure to handle with care."
  },
  {
    "label": "054",
    "text": "The most distinguishing feature of\nsaw stags are their large, curved\njaws.\nThe inner parts of their jaws look\nlike saws, which is how they get\ntheir moniker.\nWhen they feel vibrations, they stop\nmoving right away and feign death\nas a defense mechanism."
  },
  {
    "label": "055",
    "text": "Giant stags are huge stag beetles\nwith one tooth in each side of their\nlarge jaws.\nThey're very territorial but careful\nat the same time, so they usually\nhide in holes in trees.\nThey have a long life span. Records\nshow that some have lived up to\nfive years.\nThey're very popular among\ncollectors as pets, so they\ncommonly fetch a high price."
  },
  {
    "label": "056",
    "text": "Rainbow stags are called the most\nbeautiful of stag beetles because\nof their metallic-rainbow color.\nNot only are their wings a pretty\nrainbow color, but their bellies are\nas well.\nThey are quite laid back, but they\ncan be seen every now and then\nfighting to protect their territories."
  },
  {
    "label": "057",
    "text": "Cyclommatus stags are identified\nby their long pairs of mandibles,\nwhich can be as long as their bodies.\nTheir large mandibles are actually\nalready formed when they're in the\npupal stage of life.\nAlthough these beetles are mostly\neasygoing, those living in mountains\nreally can't stand the heat."
  },
  {
    "label": "058",
    "text": "Golden stags are named after their\nunique gold color and are highly\nprized by collectors.\nThey shine like gold when their\nbodies are dry, but when it's humid,\nthey actually look black.\nGolden stags have short mandibles\nthat aren't good for fighting, but\nmales sometimes fight for territory.\nThe males are surprisingly friendly\nto the females, which gives these\nbeetles a likable quality."
  },
  {
    "label": "059",
    "text": "Horned dynastids are part of the\nscarab family. Their horns are\nactually part of their exoskeleton.\nHorned dynastids don't have a nose\nor ears, but they can sense smells\nwith their antennae.\nThey are also able to feel the minor\nvibration of sounds traveling in the\nair using thin hair on their bodies.\nThey are fairly loud when flying,\nplus they emit a rather sour odor,\nso they are hard not to notice."
  },
  {
    "label": "060",
    "text": "Horned atlases, though perhaps not\nthe largest, are still known as the\nstrongest of the dynastid beetles.\nTheir 3 horns make them look like\na mash-up of a dynastid beetle\nand a stag beetle!\nIn the pupal stage, they already\nhave 3 horns and tend to be\npretty strong."
  },
  {
    "label": "061",
    "text": "Horned elephants are some of the\nheaviest dynastid beetles in the\nworld.\nThe thin yellow hair that covers\ntheir bodies makes them rather\nattractive to mates and collectors.\nTheir horns resemble an elephant\nraising its trunk, which is how they\ngot their name.\nThey're already big in their larval\nstage, but by their pupal stage, they\nare the size of a person's hand."
  },
  {
    "label": "062",
    "text": "Horned herculeses are the largest\nbeetles in the world, reaching over\n17 cm in exceptional cases.\nTheir 2 long horns look pretty,\nbut they're powerful and can hurt\na lot if you're pinched by them.\nThe longer horn is covered with\nshort, soft yellow hair, which helps\nthem to sense vibrations.\nThe yellow on their bodies turns\nblack when it's humid, making them\ndifficult to spot on humid evenings."
  },
  {
    "label": "063",
    "text": "Goliath beetles, named after the\nlegendary giant, are among the\nlargest insects in the world.\nThe larvae can weigh four ounces or\nmore. Once mature, they are only\nhalf that weight, however. \nThey spend their days flying from\ntree to tree to feed on tree sap or\nfruits."
  },
  {
    "label": "064",
    "text": "Fleas are tiny insects that feed on\nblood from humans and animals,\nwhich then results in an itchy rash.\nThey use their piercing mouths to\nsuck blood while injecting saliva\ninto their host at the same time.\nUnlike mosquitoes, both males and\nfemales suck blood. The females\nare larger than the males."
  },
  {
    "label": "065",
    "text": "Pill bugs live in moist places, which\nis why you will commonly find them\nunder rocks and in stumps.\nWhen they're touched, they roll into\na ball. Oddly enough, they're part of\nthe crustacean family.\nAs such, and since they don't like\ndry places, they can travel for a\nshort time underwater if need be."
  },
  {
    "label": "066",
    "text": "Mosquitoes can easily be found\nflying around on warm summer\nnights looking for targets.\nOnly the females feed on blood, and\nwhen they do so, they also inject\nsaliva.\nThe saliva is actually what causes\nthe annoying itchy feeling, rather\nthan the bite itself.\nMosquitoes detect body heat,\nsweat, and carbon dioxide, so they\nare drawn to exercising humans."
  },
  {
    "label": "067",
    "text": "Flies are often seen rubbing their\nforelegs to clean them, which seems\nodd, because, well, they're flies.\nHowever, they need to keep their\nlegs clean as they contain taste and\ntactile receptors.\nBy doing so, they can better savor\nthe flavor of whatever they find\nto eat next.\nThe legs also contain a sticky liquid\nso flies can more easily land on\nsmooth surfaces."
  },
  {
    "label": "068",
    "text": "The house centipede has many\nremarkably long legs that give it\na very intimidating appearance.\nHowever, despite this intense look,\nthey are for the most part harmless\nto humans.\nWhen an enemy is holding down one\nof its legs, the house centipede will\ndrop that leg to make an escape.\nThe dropped leg will continue to\nflail for a while, though, providing\na distraction."
  },
  {
    "label": "069",
    "text": "Despite the name, centipedes have a\nvarying number of legs, from about\n30 to 46 rather than 100.\nThey also have forcipules, which\nare a modification of the first pair\nof legs, that inject venom.\nCentipedes are sometimes used as\nan ingredient in herbal medicine,\nthough the effect is questionable."
  },
  {
    "label": "070",
    "text": "Spiders emit silk from their\nspinnerets and use it to create\nelaborate webs to snag prey.\nThe horizontal threads are sticky to\nthe touch, allowing them to be used\nas very strong restraints.\nHowever, the vertical threads are\nsmooth, allowing spiders to walk on\nthem without getting stuck."
  },
  {
    "label": "071",
    "text": "Tarantulas are large arachnids that\nlive in warm areas. They're some-\ntimes kept as pets.\nThey have sharp fangs, and while it\nis painful if you're bitten by one,\nthey aren't deadly to humans.\nWhat's scarier is the hair they kick\noff of their abdomens to protect\nthemselves.\nThe hair can cause serious itching\nand rashes wherever it sticks and\ncan be rather difficult to remove."
  },
  {
    "label": "072",
    "text": "Scorpions resemble crabs and are\nin the class arachnida, though they\naren't aquatic.\nMost species possess venom, but\nof the 1,000 or more species, only\n25 are deadly to humans.\nScorpion eggs hatch inside the\nfemale's abdomen, and the young\nspend time on her back after birth."
  }
]