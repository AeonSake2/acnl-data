﻿<?xml version="1.0" encoding="utf-16"?>
<msbt>
  <message label="001">Seaweed can be over three feet
long. When its spores attach to
rocks, they take root and grow.
Seaweed is naturally brown but
turns to the dark-green color we
associate it with when cooked.
It tends to spoil easily when picked,
so various preservation methods
have been developed as a necessity.
Such methods of preservation are
sprinkling ash and drying it out, or
boiling it and adding salt.</message>
  <message label="002">Sea grapes are small spheres
attached to slender stems on
branches, similar to normal grapes.
However, despite looking like and
being called &amp;quot;grapes,&amp;quot; they aren&apos;t
much like the land-based variety.
They grow in warm oceans and are
very susceptible to cold conditions,
meaning refrigeration is difficult.</message>
  <message label="003">Sea urchins are a close relative of
both starfish and sea cucumbers
and are assumed to be stationary.
In actuality, they are capable of
walking thanks to the many skinny
legs they have between their spikes.
They feed on seaweed on the ocean
floor using their strong mouths
located on their undersides.
They are rich in nutrients but also
have a lot of cholesterol, so take
care not to eat too much.</message>
  <message label="004">They attach to hard places, and
though they look like bivalves, they
are related to shrimp and crab.
Some species are even edible and
have a very crab-like taste to them,
making them a fine delicacy.
Young acorn barnacles come from
eggs, do not have shells, and float
around the ocean like shrimp.
They gradually develop their
shells as they grow into the
image we usually see.</message>
  <message label="005">Oysters are sometimes referred to
as &amp;quot;the milk of the sea&amp;quot; because of
their high nutritional value.
Young oysters come from eggs,
attach themselves to hard places
in the ocean, and then grow.
They can change gender, allowing
them to reproduce regardless of
their neighbors&apos; genders.</message>
  <message label="006">They are related to spiral shells.
Also, they have lids that cover the
openings of their shells.
When they walk, they remove the lid
and move in the shell by shuffling
their feet, much like snails.
They become active at night and
eat seaweed. They are delicious
when cooked in the shell.</message>
  <message label="007">Abalones grow less than an inch a
year, so it takes several years for
them to grow to full size.
They are related to ear shells and
have been used in celebrations and
offerings since ancient times.</message>
  <message label="008">They resemble young abalone, as
they are directly related to them,
though they act more like snails.
They live in various habitats, but
being snaillike, they actually have
feelers to get around the ocean.</message>
  <message label="009">Bivalves shaped like chestnuts,
they live in shallow waters and hide
in sand when predators approach.
They secrete a viscous liquid that
gets pulled along by the current and
enables long-distance travel.
They are popular both roasted in
the shell and cooked in a variety
of soups and chowders.</message>
  <message label="010">Pearls are formed when mostly
calcium carbonate and protein
envelop foreign substances.
Pearl oysters have numerous
threadlike feet sticking out
from their shell opening.
These allow them to latch on to
surrounding rocks and are actually
surprisingly strong.
Their adductor muscles are edible
and are considered a delicious
ingredient in some foods.</message>
  <message label="011">Scallops inhabit the cool ocean bed.
They swim by rapidly clapping
together their shell valves.
The black dots on scallops, known
as wavy &amp;quot;strings,&amp;quot; are actually their
eyes.
These eyes number over 80 but
only really function as a means
to sense light rather than motion.
Often grilled and buttered, scallops
have become a tasty seafood
enjoyed all over the world.</message>
  <message label="012">Sea anemones make their homes by
attaching to rocks and coral on the
bottom of the sea.
They use their venomous tentacles
to stun fish and shrimp to the point
of paralysis and then devour them.
The only species of fish with a
natural resistance to the venom
is the clown fish.
Sea anemones and the clown fish
nesting in their tentacles make for
a rather striking ocean-floor sight.</message>
  <message label="013">These star-shaped creatures have
hundreds of tiny feet attached to
their mobile arms.
This allows them to slowly walk and
open shellfish for food, making
them effective predators.
If an arm gets caught and ripped off
by another predator, the sea star&apos;s
arm will just grow back.
Some sea stars eat by extending
their stomachs out of their mouths
to trap and then digest prey.</message>
  <message label="014">Sea cucumbers are similar to sea
urchins and sea stars but are far
more slender and softer.
When threatened by a predator,
they expel part of their internal
organs as a natural defense.
This is to blind and entangle the
enemy so the sea cucumber has
a window of escape.
Some types of sea cucumbers allow
predators to feed on their
internal organs while they escape.
Don&apos;t shed a tear for them, though,
as the internal organs grow back
after a while. </message>
  <message label="015">Although they have no shells, sea
slugs are more closely related to
spiral shells than anything else.
Their vivid colors and patterns and
variety of shapes and sizes make
them popular with divers.
Sea slugs are hermaphrodites, so
they are all capable of mating with
one another, regardless of gender.</message>
  <message label="016">Flatworms are flat creatures, and
while some types look like sea slugs,
they have little else in common.
Some types are venomous and
produce a poison similar to the
kind blowfish are famous for.
Flatworms are hermaphrodites and
mate by fighting to determine the
loser, who then bears the eggs.</message>
  <message label="017">Mantis shrimp have long, sickle-
shaped legs and thick carapaces
covering both their bodies and tails.
They strike captured prey with
claws powerful enough to crack the
shells of even the toughest crabs.
They are a popular dish with fans
of seafood, though females with
eggs are worth more than males.</message>
  <message label="018">Also called &amp;quot;northern shrimp,&amp;quot; they
have a life span of about 5 years,
in which they are male for half.
After that time, the shrimps change
genders and become females, a
rather unique feature of the species.
The females are a popular sashimi
dish, but they don&apos;t instantly have
their characteristic sweetness.
That sweetness actually comes out
after they&apos;re left to sit for a day,
making them a dish of patience.</message>
  <message label="019">These shrimp inhabit shallow areas
of the sea and are nocturnal, hiding
under sand during the day.
By night, they go out and hunt
shellfish before returning to the
safety of their sand.
Unlike other shrimp, they don&apos;t
carry their eggs around with them
in their abdomen.
They are famous for being a tasty
variety of shrimp and are perfect
for tempura and sushi.</message>
  <message label="020">The representative species for all
large lobsters, they&apos;ve become a
lasting symbol of longevity.
They live between rocks in shallow
waters with octopuses as their
natural enemies.
Only octopuses are capable of
reaching in between the rocks with
their long arms to catch them.</message>
  <message label="021">Lobsters live in the ocean, have
large, powerful claws, and are
closely related to crawfish.
They are a delicacy, also known by
the names &amp;quot;crawfish of the sea&amp;quot; or
&amp;quot;homard,&amp;quot; their French name.
However, unlike crawfish, they are
much bigger and can live for more
than a hundred years.
Lobsters are aggressive and will
pinch with their claws if you don&apos;t
properly bind them while handling.</message>
  <message label="022">Snow crabs live in the deep sea and
are known mostly as a highly prized
winter delicacy.
When cooked, the typically dark-red
shell turns to the beautiful bright-
red color most commonly seen.
They have a life span of 20 years
and gradually grow bigger while
periodically molting.
If a leg is lost, it will grow back
when molting occurs. The female
is about half the size of the male.</message>
  <message label="023">Horsehair crabs get their name
from their spiky hairs, which can
hurt, though they have soft shells.
They inhabit the bottom of fairly
shallow waters, with their biggest
predator being the octopus.
They camouflage themselves by
putting dirt on their hair so they
don&apos;t stand out in the sand.
Their shells house a tasty portion
of innards, so this species is a
favorite of those who enjoy crab.</message>
  <message label="024">These are one of the most popular
and sought-after species of crab
in the world.
They have eight legs, including their
claws, but also two small legs under
their carapace, making 10 in all.
At full size, their leg span can easily
reach more than one yard wide,
making them big ocean dwellers.
Their thick legs are packed with
meat, making them a prized catch
for many fishers around the world.</message>
  <message label="025">Known for their long legs, adult
male spider crabs can have leg
spans of over three yards.
This actually makes them the
largest species of crab in the
world.
They are thought to have lived in
ocean bottoms for tens of millions
of years without changing shape.
Their vivid-orange bodies feature
white spots, making their coloration
truly beautiful.</message>
  <message label="026">Many people think octopuses have
eight legs, but those are not legs.
Their tentacles are actually arms.
They have a strange anatomy, with
their head located between their
body and arms.
The flexible arms are perfect for
catching all kinds of crab, shrimp,
and shellfish, especially in reefs.
When attacked by predators,
octopuses emit a black ink that
spreads through the water.
This greatly reduces visibility and
allows them to escape, making them
highly skilled at disappearing.</message>
  <message label="027">Spotted garden eels get their name
from the black spots on their
otherwise white bodies.
They nest in the sandy seafloor and
poke their heads out to catch the
plankton drifting in the current.
They&apos;re rather shy, as they
quickly dart back into their hole
at the slightest hint of danger.</message>
  <message label="028">These creatures are related to both
octopuses and squids and originated
about 60 million years ago.
They have air and body fluid in their
shells and move by adjusting the
amount of body fluid inside.
This allows them to either float or
sink depending on the situation
they find themselves in.
They have nearly 90 tentacles that
can stick out from under their shells
to grab on to rocks or prey.</message>
  <message label="029">Horseshoe crabs are called &amp;quot;living
fossils,&amp;quot; as they have not evolved
since the age of the dinosaurs.
Their backs are covered by a hard
shell, forming a simple silhouette
with their flat backs and long tails.
Though referred to as crabs, they&apos;re
actually more closely related to
spiders and scorpions.</message>
  <message label="030">Giant isopods are related to pill
bugs, and they can grow up to
20 inches long.
They are scary looking but still
come across as humorous for
some odd reason.
Despite their roly-poly builds, they
are surprisingly fast swimmers,
making them rather agile.
They have big appetites, gaining the
title &amp;quot;sea cleaners&amp;quot; mostly because
they eat dead fish on the seafloor.</message>
</msbt>