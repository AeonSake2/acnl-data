﻿<?xml version="1.0" encoding="utf-16"?>
<msbt>
  <message label="001">Amber is simply fossilized tree sap,
formed and hardened over many
thousands of years.
Prized for its beauty, amber is
often used in accessories such
as necklaces and earrings.
Amber containing a trapped insect,
such as a bee or ant, can fetch
a particularly high price.</message>
  <message label="002">Ammonites were sea creatures with
shells ranging from a few inches to
a few feet in diameter.
However, theories speculate that
giant varieties existed as well, with
shells six feet in diameter.
Oddly, the ammonite is a closer
relative to the squid or octopus
than to the chambered nautilus.</message>
  <message label="003">Fossilized dung might not sound
too exciting, but coprolites are a
treasure trove of information.
If they contain seeds, bones, or
scales, you can gain insight into
the animal&apos;s natural diet.
Dung doesn&apos;t easily turn into fossils,
so they&apos;re considered quite precious
and hard to come by.</message>
  <message label="004">Dinosaur eggs vary in size depending
on the species, but larger ones can
reach about a foot in length.
If the egg was fossilized when it
was just about to hatch, sometimes
intact bones can be found inside.
However, it is generally rather
difficult to determine the exact
species a fossilized egg belongs to.</message>
  <message label="005">Originally, all plants on the planet
were actually sea dwelling, but then
they eventually spread to land.
This fossilized fern is thought to be
one of the very first plants to make
this move to dry land.
Reproducing via spores rather than
seeds, it resembles modern ferns,
though several yards taller.</message>
  <message label="006">Unlike other fossils, fossilized foot-
prints give us a glimpse of how the
creature in question actually lived.
Thanks to modern science, we can
determine quite a lot about the
particular gaits of some creatures.
This includes how they supported
their weight, their posture when
walking, and even walking speed.</message>
  <message label="007">The archaeopteryx is considered a
close relative to the ancestors of
modern birds.
Despite the similarities, its teeth
and three-clawed hands contain 
marked differences.
As its bones were hollow to keep
its body lighter and allow for flight,
fossils rarely survive.</message>
  <message label="008">Peking man was a hominid existing
500,000 years ago who used stone
tools for crafting.
He also hunted for animals, making
him more a carnivore than an
herbivore.
Though not a direct ancestor of
modern-day humans, interaction
between the two was very likely.</message>
  <message label="009">This fossilized shark tooth has a
distinctive serrated edge, like a
steak knife. 
Sharks have existed since before
the dinosaurs, but their appearance
seems to have hardly changed at all.
Their age actually puts them at a
time even before plants had fully
propagated over every continent.</message>
  <message label="010">Similar in appearance to a wood
louse, the trilobite was an ancient
sea-dwelling arthropod.
It could range between 4 mm and
70 cm in length, depending on
various factors.
The trilobite was one of the first
creatures to have eyes and the
ability to detect enemies and prey.</message>
  <message label="011">The famous tyrannosaur was one of
the largest and strongest carnivores
of its day.
Though it could reach more than
30 feet in length, it was capable
of speeds near 20 mph.
Its hind claws were powerful, as
were its razor-sharp teeth. This
made the T. rex fearsome indeed.</message>
  <message label="012">The triceratops was a very large
herbivore, commonly identified by
its three horns and large bone frill.
Its horns were likely used for turf
wars and for fighting over females
rather than for protection.
While it&apos;s true that it most likely
lived in the same era as the T. rex,
the two probably didn&apos;t fight much.</message>
  <message label="013">The mammoth was a giant, woolly
elephant-like animal equipped with
long, deadly tusks.
Aside from protection, the tusks
were also likely used to clear paths
through the snow.
One theory states that mammoths
became extinct at the end of the
last ice age due to a lack of food.</message>
  <message label="014">The ankylosaurus is famous for the
distinctive armor that covered its
body from head to toe.
This armor consisted of large bone
knobs and plates covered in a thick,
leathery skin.
When threatened by carnivorous
dinosaurs, it would swing its club-
like tail to protect itself.</message>
  <message label="015">With its impossibly long neck and
tail, many questions exist regarding
the lifestyle of the apatosaurus.
Herbivorous dinosaurs tended to
have larger digestive tracts than
carnivorous dinosaurs.
This distinction is what usually
accounted for a thicker body,
which the apatosaurus had.
You might also hear it called a
brontosaurus, though this
name is scientifically redundant.</message>
  <message label="016">Despite its reptilian appearance,
the dimetrodon was not actually
a dinosaur, as you&apos;d assume.
It was, in fact, a mammal-like
reptile that went extinct before
the first dinosaurs appeared.
It had knife-shaped teeth for
slicing, as well as smaller ones for
crushing.
The large fan on its back was used
to regulate body temperature—
an important feature of the time.</message>
  <message label="017">The iguanodon is so named because
it was mistakenly believed to be a
large iguana.
An herbivore, it is famous for its
spikelike thumb claws, which seem
somewhat odd on a nonpredator.
Supposedly it would run on two
legs but would use all four when
it was just walking at its leisure.</message>
  <message label="018">The sabertooth tiger was about the
size of a lion and is famous for its
two long canine teeth.
It would prey mostly on woolly
mammoths, but climate change
made that difficult.
Eventually, the changing climate and
competition with humans for food
drove these creatures to extinction.</message>
  <message label="019">The pachycephalosaurus is known
for its round, helmetlike head, the
use of which is still debated.
The bony dome atop its skull was
nearly a foot thick, which is why
mostly skull fossils survive.
Some theories say the animals
would smash their heads together
to determine pecking order.</message>
  <message label="020">The parasaur is well known for the
distinctive head crest extending
from its nose to behind its head.
The crest was formed from a
hollow bone that was more than
a yard in length.
Theories hold that it was used to
amplify the creature&apos;s vocalizations
for a variety of reasons.</message>
  <message label="021">The diplodocus was an herbivore
of the late Jurassic period sporting
a tremendously long tail and neck.
The neck was so long that, from
snout to tail, its length could
reach a staggering 175 feet long.
Evidence suggests it would use its
long, slender tail like a whip to
defend itself from attackers.</message>
  <message label="022">While not a dinosaur itself, the
plesiosaur, a marine reptile, did
live alongside the dinosaurs.
These creatures could reach lengths
of 23 feet or longer, the bulk of
that being their long necks.
Plesiosaurs are commonly mistaken
for certain kinds of mythical sea
monsters, such as &amp;quot;Nessie.&amp;quot;</message>
  <message label="023">The name stegosaurus literally
means &amp;quot;roofed lizard&amp;quot; or &amp;quot;covered
lizard.&amp;quot;
This name came from a belief at
the time of discovery that it was
covered in bony plates like a shell.
We know now that the plates stood
fully upright along its back and were
covered in thick skin.
Beyond protection from harm, they
were likely used to regulate body
temperature.</message>
  <message label="024">Pteranodons were a type of winged
reptile, perhaps the ancestors of
common birds.
With their wings spread, they were
about the size of a small airplane,
but their bodies were very light.
Strangely, they weighed less than
50 pounds, with wings made from
skin, allowing for ease of flight.</message>
  <message label="025">Though they bear a striking visual
similarity to dolphins, ichthyosaurs
were reptiles rather than mammals.
Interestingly, adult specimens have
been found with juvenile bones
inside their abdominal cavities.
This suggests that rather than laying
eggs, ichthyosaurs gave birth to live
young in the water.</message>
  <message label="026">Usually just called raptors, they&apos;re
also known as velociraptors, which
literally means &amp;quot;swift seizers.&amp;quot;
They had large heads for their
bodies, so theories suggest they
were relatively intelligent.
Though not imposing in stature,
they had large and deadly claws
on their hind legs.</message>
  <message label="027">Styracosaurus literally means
&amp;quot;spiked lizard.&amp;quot; It looked roughly
like a more intense triceratops.
In addition to the spike on its nose,
it had three sets of horns extending
from its gaudy neck frill.
Fossils have generally been found
in groups, so the belief is that it
was a herd animal.</message>
  <message label="028">The spinosaurus is famous for the
sail on its back, made up of skin
stretched over spikelike bones.
The assumption is that these sails
were used to help regulate body
temperature, common for reptiles.
In addition to having this large
sail, it is also the largest of the
carnivorous dinosaurs.
Despite being so commanding, its
diet most likely consisted of fish
from swampy, marshy regions.</message>
  <message label="029">The megacerops was a mammalian
grazer with a Y-shaped horn on its
nose.
It is a distant relative of the
rhinoceros, which the horn
helps give away.
Its main source of food was soft
grass, but climate change caused
this food source to disappear.
Ultimately, without a thriving source
of fresh food, megacerops joined
other creatures in extinction.</message>
  <message label="030">With a name that literally means
&amp;quot;ruler turtle,&amp;quot; the archelon was an
ancient species of sea turtle.
Reaching 13 feet in length and two
metric tons in weight, it was a giant
that lived up to its name.
It likely ate jellyfish and squid,
though theories say it may have
also preyed on ammonites.
Unlike with modern turtles, its shell
was too soft to allow it to pull in its
limbs for a defensive position.</message>
</msbt>